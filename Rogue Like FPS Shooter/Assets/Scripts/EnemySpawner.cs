using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
	[SerializeField] private GameObject[] _enemyPrefabs;
	[SerializeField] private Color _gizmoColor = Color.red;
	[SerializeField, Min(0)] private int _count;
	[SerializeField, Range(0f, 100f)] private float _radius;

	[Space]
	[SerializeField] private bool _respawn = false;

	[SerializeField] private float _respawnTime = 5;

	private GameManager _gameManager;

	// Start is called before the first frame update
	private void Start()
	{
		_gameManager = GameManager.instance;
		_gameManager.onNoEnemyLeft += HandleOnNoEnemyLeft;
		Spawn();
	}

	private void HandleOnNoEnemyLeft()
	{
		if (_respawn)
		{
			StartCoroutine(IESpawn(_respawnTime));
		}
	}

	private void Spawn()
	{
		StartCoroutine(IESpawn());
	}

	private IEnumerator IESpawn(float delayBeforeStart = 0f)
	{
		yield return new WaitForSeconds(delayBeforeStart);

		for (int i = 0; i < _count; i++)
		{
			float delay = Random.Range(0.01f, 0.5f);
			yield return new WaitForSeconds(delay);

			// Position inside a sphere with radius {_radius} and the center at zero
			Vector3 pos = Random.insideUnitSphere * _radius;
			pos.y = transform.position.y;

			// Add offset
			pos += transform.position;

			// Spawn GameObject
			Quaternion rotation = Quaternion.Euler(0, Random.Range(0.0f, 360.0f), 0);
			int randomIndex = Random.Range(0, _enemyPrefabs.Length);
			GameObject go = Instantiate(_enemyPrefabs[randomIndex], pos, rotation) as GameObject;
			go.transform.parent = transform;
		}
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = new Color(1, 1, 1, 0.25f) * _gizmoColor;
		Gizmos.DrawSphere(transform.position, _radius);
		Gizmos.DrawWireSphere(transform.position, _radius);
	}
}