using System;
using UnityEngine;

[Serializable]
public class Projectile
{
	public string name = "Name";
	public int damage = 10;
	[Min(0)] public float size = 1;
	public int speed = 2000;
	public float reloadTime = 1;
	public GameObject prefab;
}