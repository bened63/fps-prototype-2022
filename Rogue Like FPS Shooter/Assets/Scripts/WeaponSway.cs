using System;
using UnityEngine;

public class WeaponSway : MonoBehaviour
{
	[SerializeField] private float _smooth = 11;
	[SerializeField] private float _swayFactor = 15;
	[SerializeField, Min(0f)] private float _recoilStrength = 1f;

	// Start is called before the first frame update
	private void Start()
	{
		GameManager.instance.onPlayerDeath += HandleOnPlayerDeath;
	}

	private void HandleOnPlayerDeath()
	{
		gameObject.SetActive(false);
	}

	// Update is called once per frame
	private void Update()
	{
		float mouseX = Input.GetAxisRaw("Mouse X") * _swayFactor;
		float mouseY = Input.GetAxisRaw("Mouse Y") * _swayFactor;

		Quaternion rotX = Quaternion.AngleAxis(-mouseY, Vector3.right);
		Quaternion rotY = Quaternion.AngleAxis(-mouseX, Vector3.up);

		Quaternion targetRot = rotX * rotY;

		transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRot, _smooth * Time.deltaTime);
	}

	public void Recoil()
	{
		Vector3 rot = transform.localEulerAngles;
		rot.x = -1f * _recoilStrength;
		transform.localEulerAngles = rot;
	}
}