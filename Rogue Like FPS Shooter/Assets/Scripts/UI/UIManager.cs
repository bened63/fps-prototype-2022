using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	#region Singleton Handling =======================================================================================

	private static UIManager _instance;

	public static UIManager instance
	{ get { return _instance; } }

	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		}
		else
		{
			_instance = this;
		}
	}

	#endregion Singleton Handling =======================================================================================

	[SerializeField] private Healthbar _healthbar;
	[SerializeField] private Text _enemyCountText;
	[SerializeField] private Text _missionText;
	[SerializeField] private BlackScreen _blackScreen;
	[SerializeField] private Text _deathText;

	private void Start()
	{
		GameManager.instance.onEnemyCountChanged += (count) => SetEnemyCountText(count);
		GameManager.instance.onGameStarted += HandleOnGameStarted;
		GameManager.instance.onFinishReached += HandleOnFinishReached;
		GameManager.instance.onPlayerDeath += HandleOnPlayerDeath;
	}

	public void SetHealth(float health)
	{
		_healthbar.value = health;
	}

	public void SetEnemyCountText(float count)
	{
		_enemyCountText.text = string.Format("Enemy Count: {0}", count);
	}

	private void HandleOnGameStarted()
	{
		_blackScreen.FadeOut(0.5f, 1f);
		_missionText.GetComponent<UIFader>().FadeIn(0.1f, 3.5f);
		_missionText.GetComponent<UIFader>().FadeOut(0.2f, 6f);
	}

	private void HandleOnFinishReached()
	{
		_blackScreen.FadeIn(0.5f, 1f);
	}

	private void HandleOnPlayerDeath()
	{
		_deathText.GetComponent<UIFader>().FadeIn(0.1f, 1.5f);
	}
}