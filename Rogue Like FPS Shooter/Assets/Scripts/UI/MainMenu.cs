using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	[SerializeField] private string _startSceneName;

	private void Start()
	{
		Cursor.visible = true;
	}

	public void LoadLevel(int i)
	{
		StartCoroutine(IELoadAsyncScene());
	}

	public void LoadFirstLevel()
	{
		StartCoroutine(IELoadAsyncScene());
	}

	public void Exit()
	{
		Application.Quit();
	}

	private IEnumerator IELoadAsyncScene()
	{
		// The Application loads the Scene in the background as the current Scene runs.
		// This is particularly good for creating loading screens.
		// You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
		// a sceneBuildIndex of 1 as shown in Build Settings.

		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(_startSceneName);

		// Wait until the asynchronous scene fully loads
		while (!asyncLoad.isDone)
		{
			yield return null;
		}
	}
}