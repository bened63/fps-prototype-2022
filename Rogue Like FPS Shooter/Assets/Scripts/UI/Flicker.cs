using UnityEngine;
using UnityEngine.UI;

public class Flicker : MonoBehaviour
{
	[SerializeField, Min(0f)] private float _speed = 1;
	[SerializeField, Range(0f, 2f)] private float _strength = 1;
	[SerializeField] private AnimationCurve _curve;
	private Image _image;
	private Color _color;
	float _angle = 0;

	// Start is called before the first frame update
	private void Start()
	{
		_image = GetComponent<Image>();
	}

	// Update is called once per frame
	private void Update()
	{
		_angle += Time.deltaTime * _speed;
		if (_angle >= 360) _angle = 0;

		float t = Mathf.Sin(_angle);
		t = 0.5f * (t + 1f);
		_color = _image.color;
		_color.a = Mathf.Clamp01(_strength * _curve.Evaluate(t));
		_image.color = _color;
	}
}