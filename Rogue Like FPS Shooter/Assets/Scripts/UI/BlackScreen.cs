using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class BlackScreen : MonoBehaviour
{
    private CanvasGroup _canvasGroup;

    // Start is called before the first frame update
    void Start()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
		_canvasGroup.alpha = 1;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void FadeIn(float duration, float delay = 0f)
    {
		StartCoroutine(IEFade(true, duration, delay));
    }

    public void FadeOut(float duration, float delay = 0f)
    {
		StartCoroutine(IEFade(false, duration, delay));
    }

    private IEnumerator IEFade(bool show, float duration, float delay)
	{
		float currentLerpTime = 0f;
		float lerpTime = duration;
		bool run = true;
		float start = show ? 0 : 1;
		float end = show ? 1 : 0;

		yield return new WaitForSeconds(delay);

		while (run)
		{
			//increment timer once per frame
			currentLerpTime += Time.deltaTime;
			if (currentLerpTime > lerpTime)
			{
				currentLerpTime = lerpTime;
			}

			//lerp!
			float t = currentLerpTime / lerpTime;
			t = t * t * t * (t * (6f * t - 15f) + 10f);
			_canvasGroup.alpha = Mathf.Lerp(start, end, t);

			// Finish
			run = currentLerpTime < lerpTime;

			yield return null;
		}
	}
}
