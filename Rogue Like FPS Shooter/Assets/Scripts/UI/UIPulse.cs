using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class UIPulse : MonoBehaviour
{
	[SerializeField] private float _speed = 1f;
	private CanvasGroup _canvasGroup;
	private float f = 0f;

	// Start is called before the first frame update
	private void Start()
	{
		_canvasGroup = GetComponent<CanvasGroup>();
	}

	// Update is called once per frame
	private void Update()
	{
		f += Time.deltaTime * _speed;
		if (f >= 360) f = 0f;
		float t = Mathf.Sin(Mathf.Deg2Rad * f);
		_canvasGroup.alpha = Mathf.Lerp(0f, 1f, t);
	}
}