using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	#region Singleton Handling =======================================================================================

	private static GameManager _instance;

	public static GameManager instance
	{ get { return _instance; } }

	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		}
		else
		{
			_instance = this;
		}
	}

	#endregion Singleton Handling =======================================================================================

	[SerializeField] private string[] levelSequence;

	[SerializeField] private string currentLevelName;
	private float _enemyCount;

	public float enemyCount

	{
		get => _enemyCount;
		set
		{
			_enemyCount = Mathf.Clamp(value, 0, float.PositiveInfinity);
			onEnemyCountChanged?.Invoke(_enemyCount);

			if (_enemyCount == 0)
			{
				onNoEnemyLeft?.Invoke();
			}
		}
	}

	private bool _playerAlive;

	public bool playerAlive

	{
		get => _playerAlive;
		set
		{
			_playerAlive = value;
			if (!_playerAlive) onPlayerDeath?.Invoke();
		}
	}

	private bool _disableInput;

	public bool disableInput

	{
		get => _disableInput;
		set
		{
			_disableInput = value;
			onDisableInput?.Invoke(_disableInput);
		}
	}

	public Vector3 start { get; set; }
	public Vector3 finish { get; set; }

	private bool _pause = false;

	public bool pause
	{
		get => _pause;
		set
		{
			_pause = value;
			onGamePaused?.Invoke(_pause);
		}
	}

	public event Action<GameObject> onProjectileHit;

	public event Action<float> onEnemyCountChanged;

	public event Action onNoEnemyLeft;

	public event Action onPlayerDeath;

	public event Action<bool> onDisableInput;

	public event Action onGameStarted;

	public event Action onFinishReached;

	public event Action<bool> onGamePaused;

	// Start is called before the first frame update
	private void Start()
	{
		Cursor.visible = false;
		StartCoroutine(Init());
	}

	private void Update()
	{
		if (!_playerAlive && Input.GetKeyDown(KeyCode.R))
		{
			SceneManager.LoadScene("Level_Core");
			StartCoroutine(Init());
		}

		if (_playerAlive && Input.GetKeyDown(KeyCode.Escape))
		{
			pause = !pause;
			if (pause) Time.timeScale = 0;
			else Time.timeScale = 1;
		}
	}

	private IEnumerator IELateStart()
	{
		yield return new WaitForEndOfFrame();
		onEnemyCountChanged?.Invoke(_enemyCount);
		StartGame();
	}

	public void StartGame()
	{
		disableInput = false;
		onGameStarted?.Invoke();
	}

	public void FinishReached()
	{
		disableInput = true;
		onFinishReached?.Invoke();
		NextScene();
	}

	private IEnumerator Init()
	{
		// The Application loads the Scene in the background as the current Scene runs.
		// This is particularly good for creating loading screens.
		// You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
		// a sceneBuildIndex of 1 as shown in Build Settings.

		if (SceneManager.sceneCount == 1)
		{
			AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(levelSequence[0], LoadSceneMode.Additive);
			currentLevelName = levelSequence[0];

			// Wait until the asynchronous scene fully loads
			while (!asyncLoad.isDone)
			{
				yield return null;
			}
		}

		UpdateStartAndFinishPoition();

		StartCoroutine(IELateStart());
	}

	private void UpdateStartAndFinishPoition()
	{
		// All scenes are loaded
		GameObject startGo = GameObject.FindGameObjectWithTag("Start");
		start = startGo.transform.position;

		GameObject finishGo = GameObject.FindGameObjectWithTag("Finish");
		finish = finishGo.transform.position;
	}

	private void NextScene()
	{
		int currentIndex = Array.IndexOf(levelSequence, currentLevelName);
		int nextIndex = currentIndex >= levelSequence.Length - 1 ? 0 : currentIndex + 1;
		StartCoroutine(IELoadAsyncScene(levelSequence[nextIndex]));
	}

	private IEnumerator IELoadAsyncScene(string name)
	{
		yield return new WaitForSeconds(3.3f);

		// The Application loads the Scene in the background as the current Scene runs.
		// This is particularly good for creating loading screens.
		// You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
		// a sceneBuildIndex of 1 as shown in Build Settings.

		AsyncOperation asyncUnLoad = SceneManager.UnloadSceneAsync(currentLevelName);

		// Wait until the asynchronous scene fully unloads
		while (!asyncUnLoad.isDone)
		{
			yield return null;
		}

		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
		currentLevelName = name;

		// Wait until the asynchronous scene fully loads
		while (!asyncLoad.isDone)
		{
			yield return null;
		}

		UpdateStartAndFinishPoition();

		StartGame();
	}
}