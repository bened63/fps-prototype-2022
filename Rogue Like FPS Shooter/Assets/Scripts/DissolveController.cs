using System.Collections;
using UnityEngine;

public class DissolveController : MonoBehaviour
{
	[SerializeField, Range(0f, 1f)] private float _cutoff = 0f;
	private SkinnedMeshRenderer _meshRenderer;
	private Material[] _mats;
	private Coroutine _coroutine;

	private void Start()
	{
		_meshRenderer = this.GetComponent<SkinnedMeshRenderer>();
		_mats = _meshRenderer.materials;
	}

	private void Update()
	{
		_mats[0].SetFloat("_Cutoff", _cutoff);

		// Unity does not allow meshRenderer.materials[0]...
		_meshRenderer.materials = _mats;
	}

	public void Dissolve(float duration)
	{
		if (_coroutine != null) StopCoroutine(_coroutine);
		else _coroutine = StartCoroutine(IEDissolve(duration));
	}

	private IEnumerator IEDissolve(float duration)
	{
		float lerpTime = duration;
		float currentLerpTime = 0f;
		bool run = true;

		while (currentLerpTime < lerpTime)
		{
			//increment timer once per frame
			currentLerpTime += Time.deltaTime;
			if (currentLerpTime > lerpTime)
			{
				currentLerpTime = lerpTime;
			}

			//lerp!
			float t = currentLerpTime / lerpTime;
			_cutoff = Mathf.Lerp(0, 1, t);

			yield return null;
		}
	}
}