using SensorToolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
	public enum State
	{
		Idle, Patrol, Wander, MoveToTarget, Attack, Die
	}

	[Header("References")]
	[SerializeField] private Healthbar _healthbar;

	[Header("Audio")]
	[SerializeField] private AudioClip _dieSound;

	[SerializeField] private AudioClip _wooshSound;
	[SerializeField, Range(0f, 1f)] private float _volume = 1;

	[Header("Settings")]
	[SerializeField] private float _damage = 10f;

	[SerializeField] private float _healthMax = 100f;
	[SerializeField] private float _health;
	[SerializeField] private float _attackDistance = 5;
	[SerializeField] private float _hitRange = 6;
	[SerializeField] private float _rotationSpeed = 2;
	[SerializeField] private int _stunHitCount = 3;
	[SerializeField] private float _sensorRange = 15;
	[SerializeField] private float _attackSpeed = 1;
	[SerializeField] private bool _informNearbyEnemies = true;

	[Space(12)]
	[SerializeField] private Transform _target;

	private GameManager _gameManager;
	private AnimationController _animationController;
	private NavMeshAgent _agent;
	private Rigidbody _rigidbody;
	private RangeSensor _rangeSensor;
	private AudioSource _audioSource;
	private State _enemyState = State.Idle;
	private bool _isMoving = false;
	private bool _isAttacking = false;
	private bool _isStunned = false;
	private bool _isDying = false;
	private float _distanceToTarget = 0;
	private int _stunHits = 0;
	private Player _player;
	private Vector3 _positionLastFrame;
	private float _lastAttackTime = 0;

	public float health
	{
		get => _health;
		set
		{
			_health = value;
			_healthbar.value = health / _healthMax;
		}
	}

	public Transform target { get => _target; set => _target = value; }

	private void Awake()
	{
		_animationController = GetComponent<AnimationController>();
		_agent = GetComponent<NavMeshAgent>();
		_rigidbody = GetComponent<Rigidbody>();
		_rangeSensor = GetComponent<RangeSensor>();
		_player = FindObjectOfType<Player>();
		_audioSource = GetComponent<AudioSource>();
	}

	// Start is called before the first frame update
	private void Start()
	{
		health = _healthMax;
		_agent.stoppingDistance = _attackDistance;
		_gameManager = GameManager.instance;
		_gameManager.enemyCount++;
		_gameManager.onPlayerDeath += HandleOnPlayerDeath;
		_rangeSensor.SensorRange = _sensorRange;
	}

	// Update is called once per frame
	private void Update()
	{
		// Healthbar
		//_healthbar.Show(false);

		// Do nothing
		if (_isStunned || _isDying)
		{
			return;
		}

		// Set target / destination
		if (_target != null)
		{
			// Always face target
			RotateTowards(_target);

			// Set destination
			_agent.SetDestination(_target.position);
		}

		if (!_isAttacking)
		{
			if (IsMoving())
			{
				_animationController.PlayAnimation(AnimationController.AnimationState.Walk);
				//Debug.LogFormat("<color=lime>Walk</color>");
			}
			else if (!IsMoving())
			{
				_animationController.PlayAnimation(AnimationController.AnimationState.Idle);
				//Debug.LogFormat("<color=lime>Idle</color>");
			}
		}

		// SensorToolkit: Is Player in range than move to Player
		if (_gameManager.playerAlive)
		{
			_lastAttackTime += _attackSpeed * Time.deltaTime;
			if (_lastAttackTime >= 1f)
			{
				_lastAttackTime = 0f;

				List<GameObject> detected = _rangeSensor.GetDetectedByTag("Player");
				if (detected.Count > 0)
				{
					_distanceToTarget = Vector3.Distance(transform.position, detected[0].transform.position);
					//Debug.LogFormat("<color=lime>Distance: {0}</color>", distanceToPlayer);

					if (_distanceToTarget > _attackDistance)
					{
						// Move to target (Player)
						_target = detected[0].transform;
						CancelAttack();
					}
					else
					{
						// Attack target (Player)
						Attack(detected[0]);
					}
				}
				else
				{
					CancelAttack();
				}
			}
		}
	}

	private void Attack(GameObject targetGo)
	{
		//Debug.LogFormat("<color=lime>Attack target: {0}</color>", targetGo.name);
		_isAttacking = true;
		_animationController.PlayAnimation(AnimationController.AnimationState.Attack, AnimationController.AnimationState.Idle, true);
		AudioSource.PlayClipAtPoint(_wooshSound, transform.position, _volume);
		//Invoke("AttackFinished", _animationController.GetAnimationDuration(AnimationController.AnimationState.Attack));
	}

	private void CancelAttack()
	{
		//_isAttacking = true;
		//Debug.LogFormat("<color=lime>Attack target: {0}</color>", targetGo.name);
		//_animationController.PlayAnimation(AnimationController.AnimationState.Attack);
		//Invoke("AttackFinished", _animationController.GetAnimationDuration(AnimationController.AnimationState.Attack));
		_isAttacking = false;
	}

	// An event is called when the attack is performed and damage can be done
	public void AttackHandler(string s)
	{
		if (_distanceToTarget < _hitRange)
		{
			// Damage target
			if (_target.tag == "Player")
			{
				Player p = _target.GetComponent<Player>();
				p.Hit(_damage);
			}
		}
	}

	private void RotateTowards(Transform target)
	{
		Vector3 direction = (target.position - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation(direction);
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * _rotationSpeed);
	}

	private bool IsPlayerInRange()
	{
		bool result = false;

		/*
		RaycastHit hit;

		// Cast a sphere wrapping character controller 10 meters forward
		// to see if it is about to hit anything.
		if (Physics.SphereCast(transform.position, _sensorRadius, transform.forward, out hit, Mathf.Infinity))
		{
			result = hit.transform.tag == "Player";
		}
		 */

		return result;
	}

	private void Stun(float duration)
	{
		_isStunned = true;
		_animationController.PlayAnimation(AnimationController.AnimationState.Hit, AnimationController.AnimationState.Idle);

		Toolbox.Invoke(this, () =>
		{
			_stunHits = 0;
			_isStunned = false;
		}, duration);
	}

	public void Hit(GameObject hitGo, float damage)
	{
		if (health <= 0) return; // Do nothing if health is zero

		health = Mathf.Clamp(health - damage, 0, _healthMax);

		if (health > 0 && !_isStunned)
		{
			_stunHits++;
			if (_stunHits >= _stunHitCount)
			{
				Stun(_animationController.GetAnimationDuration(AnimationController.AnimationState.Hit));
			}
		}

		// Check if still alive
		if (health <= 0)
		{
			Die();
		}

		// Set new target for attacking
		_target = _player.transform;

		// Inform other nearby enemies to attack Player
		if (_informNearbyEnemies)
		{
			List<GameObject> detectedEnemies = _rangeSensor.GetDetectedByTag("Enemy");
			foreach (GameObject go in detectedEnemies)
			{
				float distance = Vector3.Distance(transform.position, go.transform.position);

				// Inform other enemies in half of sensor range of Player's position
				if (distance < 0.5f * _sensorRange)
				{
					StartCoroutine(IEInformNearbyEnemy(go));
				}
			}
		}
	}

	private IEnumerator IEInformNearbyEnemy(GameObject enemyGo)
	{
		float delay = Random.Range(0f, 1f);
		yield return new WaitForSeconds(delay);

		if (enemyGo != null)
		{
			Enemy e = enemyGo.GetComponentInParent<Enemy>();
			e.target = _player.transform;
		}
	}

	private void Die()
	{
		_isDying = true;

		// Animation
		_animationController.PlayAnimation(AnimationController.AnimationState.Die);

		// Sound
		AudioSource.PlayClipAtPoint(_dieSound, transform.position, _volume);

		// Dissolve
		DissolveController dc = transform.GetComponentInChildren<DissolveController>();
		if (dc != null)
		{
			float dur = 0.8f;
			dc.Dissolve(dur);
			Destroy(gameObject, dur);
			_gameManager.enemyCount--;
		}

		Toolbox.Invoke(this, () => { _isDying = false; }, _animationController.GetAnimationDuration(AnimationController.AnimationState.Die));
	}

	private bool IsMoving()
	{
		Vector3 velocity = Vector3.zero;
		if (_positionLastFrame != null)
		{
			var delta = (transform.position - _positionLastFrame);
			velocity = delta / Time.deltaTime;
		}
		_positionLastFrame = transform.position;
		//Debug.LogFormat("<color=lime>velocity.magnitude = {0} | {1} | {2}</color>", velocity.magnitude, velocity.magnitude > 1, gameObject.name);
		return velocity.magnitude > 1;
	}

	public void ShowHealthbar()
	{
		_healthbar.Show();
	}

	private void HandleOnPlayerDeath()
	{
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(0, 0, 1, 0.25f);
		Gizmos.DrawSphere(transform.position, _sensorRange);
		Gizmos.DrawWireSphere(transform.position, _sensorRange);
	}

	//private void OnDrawGizmosSelected()
	//{
	//	Color transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
	//	Color transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);

	//	if (IsPlayerInRange()) Gizmos.color = transparentRed;
	//	else Gizmos.color = transparentGreen;

	//	// when selected, draw a gizmo in the position of, and matching radius of, the grounded collider
	//	Gizmos.DrawSphere(transform.position, _sensorRadius);
	//}
}