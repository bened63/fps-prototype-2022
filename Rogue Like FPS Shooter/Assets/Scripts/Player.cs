using Cinemachine;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private AudioClip _hitSound;

	[SerializeField] private AudioClip _hurtSound;
	[SerializeField] private AudioClip _dieSound;
	[SerializeField] private AudioClip _beamUpSound;
	[SerializeField] private AudioClip _beamDownSound;

	[Header("Settings")]
	[SerializeField] private float _healthMax = 100f;

	[SerializeField] private float _health;

	private CinemachineImpulseSource _cinemachineImpulseSource;
	private GameManager _gameManager;
	private UIManager _uiManager;
	private Camera _cam;
	private Animator _animator;

	public float health
	{
		get => _health;
		set
		{
			_health = value;
			_uiManager.SetHealth(health / _healthMax);
		}
	}

	private void Awake()
	{
		_cinemachineImpulseSource = GetComponent<CinemachineImpulseSource>();
		_gameManager = GameManager.instance;
		_uiManager = UIManager.instance;
		_cam = Camera.main;
		_animator = GetComponent<Animator>();
	}

	// Start is called before the first frame update
	private void Start()
	{
		_gameManager.playerAlive = true;
		health = _healthMax;

		_gameManager.onDisableInput += HandleOnDisableInput;
		_gameManager.onGameStarted += HandleOnGameStarted;
	}

	private void HandleOnGameStarted()
	{
		transform.position = _gameManager.start;
		BeamDown();
	}

	// Update is called once per frame
	private void Update()
	{
		// Show healthbar
		RaycastHit hit;
		if (Physics.Raycast(_cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)), out hit))
		{
			if (hit.transform.tag == "Enemy")
			{
				Enemy e = hit.transform.GetComponent<Enemy>();
				e.ShowHealthbar();
			}
		}
	}

	public void Hit(float damage)
	{
		//Debug.LogFormat("<color=lime>Hit</color>");
		health = Mathf.Clamp(health - damage, 0f, _healthMax);
		_cinemachineImpulseSource.GenerateImpulse();
		AudioSource.PlayClipAtPoint(_hitSound, transform.position);
		AudioSource.PlayClipAtPoint(_hurtSound, transform.position);

		if (_health <= 0f && _gameManager.playerAlive)
		{
			Die();
		}
	}

	private void Die()
	{
		_gameManager.playerAlive = false;
		_gameManager.disableInput = true;
		AudioSource.PlayClipAtPoint(_dieSound, transform.position);

		_animator.enabled = true;
		_animator.SetTrigger("Die");
	}

	private void HandleOnDisableInput(bool disableInput)
	{
		UnityEngine.InputSystem.PlayerInput input = GetComponent<UnityEngine.InputSystem.PlayerInput>();
		input.enabled = !disableInput;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Finish")
		{
			_gameManager.FinishReached();
			BeamUp();
		}
	}

	private void BeamUp()
	{
		StartCoroutine(IEBeam(true, 4));
		AudioSource.PlayClipAtPoint(_beamUpSound, transform.position);
	}

	private void BeamDown()
	{
		StartCoroutine(IEBeam(false, 2.95f));
		AudioSource.PlayClipAtPoint(_beamDownSound, transform.position);
	}

	IEnumerator IEBeam(bool up, float duration)
	{
		float currentLerpTime = 0f;
		float lerpTime = duration;
		bool run = true;
		Vector3 upPos = transform.position + new Vector3(0, 30, 0);
		Vector3 start = up ? transform.position : upPos;
		Vector3 end = up ? upPos : transform.position;

		while (run)
		{
			//increment timer once per frame
			currentLerpTime += Time.deltaTime;
			if (currentLerpTime > lerpTime)
			{
				currentLerpTime = lerpTime;
			}

			//lerp!
			float t = currentLerpTime / lerpTime;
			t = 1f - Mathf.Cos(t * Mathf.PI * 0.5f);
			transform.position = Vector3.Lerp(start, end, t);

			// Finish
			run = currentLerpTime < lerpTime;

			yield return null;
		}
	}
}