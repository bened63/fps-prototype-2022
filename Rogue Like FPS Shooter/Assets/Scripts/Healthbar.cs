using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    //[SerializeField] private bool _animate = false;
    [SerializeField] private float _lerpTime = 0.1f;
    [SerializeField] private bool _worldSpace = false;

    private Slider _slider;
    private float _value;
    private float _fadeOutTimer = 0.1f;
    private Coroutine _coroutine;
    private Coroutine _fadeOutCoroutine;
    private CanvasGroup _canvasGroup;
    private Camera _cam;

	public float value 
    {
        get => _value;
        set
		{
            _value = Mathf.Clamp(value, 0f, 1f);
            if (_coroutine != null) StopCoroutine(_coroutine);
            _coroutine = StartCoroutine(IESetSliderAnimation(_value));
		}
    }

	private void Awake()
	{
        _slider = GetComponent<Slider>();
        _canvasGroup = GetComponent<CanvasGroup>();
        _cam = Camera.main;
	}

	private void Update()
    {
        if (_worldSpace)
		{
            // Healthbar should always face Player / Camera
            transform.LookAt(_cam.transform);
        }
    }

	private IEnumerator IESetSliderAnimation(float targetValue)
	{
        float lerpTime = _lerpTime;
        float currentLerpTime = 0f;
        float start = _slider.value;
        float end = targetValue;

        while (currentLerpTime < lerpTime)
		{
            //increment timer once per frame
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
            }

            //lerp!
            float t = currentLerpTime / lerpTime;
            t = t * t * t * (t * (6f * t - 15f) + 10f);
            _slider.value = Mathf.Lerp(start, end, t);

            yield return null;
        }
    }

    public void Show()
	{
        _canvasGroup.alpha = 1;
        if (_fadeOutCoroutine != null) StopCoroutine(_fadeOutCoroutine);
        _fadeOutCoroutine = StartCoroutine(IEShow());
	}

    private IEnumerator IEShow()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(_fadeOutTimer);
        _canvasGroup.alpha = 0;
	}
}
