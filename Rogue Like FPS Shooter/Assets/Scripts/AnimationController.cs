using System.Collections;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
	public enum AnimationState
	{
		None,
		Idle,
		Walk,
		Attack,
		Hit,
		Die,
	}

	[SerializeField] private Animator _animator;
	[SerializeField] private int _currentAnimationIndex;
	[SerializeField] private AnimationState _animationState;

	[Header("Animations")]
	[SerializeField] private GameObject[] _animationsIdle;
	[SerializeField] private GameObject[] _animationsWalk;
	[SerializeField] private GameObject[] _animationsAttack;
	[SerializeField] private GameObject[] _animationsHit;
	[SerializeField] private GameObject[] _animationsDie;

	private float _shotTime;
	private bool _isAnimationPlaying = false;
	private bool _isChangingAnimationState = false;
	private Coroutine _changeAnimationCoroutine;
	private float _animationTime = 0;

	public float GetAnimationDuration(AnimationState state)
	{
		float length = 0;

		switch (state)
		{
			case AnimationState.Idle:
				length = GetAnimationDuration(_animationsIdle[0].name);
				break;

			case AnimationState.Walk:
				length = GetAnimationDuration(_animationsWalk[0].name);
				break;

			case AnimationState.Attack:
				length = GetAnimationDuration(_animationsAttack[0].name);
				break;

			case AnimationState.Hit:
				length = GetAnimationDuration(_animationsHit[0].name);
				break;

			case AnimationState.Die:
				length = GetAnimationDuration(_animationsDie[0].name);
				break;

			default:
				break;
		}
		return length;
	}

	private float GetAnimationDuration(string name)
	{
		float length = 0;
		AnimationClip[] clips = _animator.runtimeAnimatorController.animationClips;
		foreach (AnimationClip clip in clips)
		{
			if (clip.name == name)
			{
				length = clip.length;
				break;
			}
		}
		return length;
	}

	public void PlayAnimation(AnimationState state, AnimationState callback = AnimationState.None, bool forceRestart = false)
	{
		if (!forceRestart && state == _animationState)
		{
			return;
		}

		switch (state)
		{
			case AnimationState.Idle:
				ChangeAnimation(_animationsIdle[0].name, callbackAnimation: callback);
				break;

			case AnimationState.Walk:
				ChangeAnimation(_animationsWalk[0].name, callbackAnimation: callback);
				break;

			case AnimationState.Attack:
				ChangeAnimation(_animationsAttack[0].name, callbackAnimation: callback);
				break;

			case AnimationState.Hit:
				ChangeAnimation(_animationsHit[0].name, callbackAnimation: callback);
				break;

			case AnimationState.Die:
				ChangeAnimation(_animationsDie[0].name, callbackAnimation: callback);
				break;

			default:
				break;
		}
		_animationState = state;
	}

	private void ChangeAnimation(string name, float fadeDuration = 0.125f, bool loop = false, AnimationState callbackAnimation = AnimationState.None)
	{
		if (_changeAnimationCoroutine != null)
		{
			StopCoroutine(_changeAnimationCoroutine);
		}
		_changeAnimationCoroutine = StartCoroutine(IEChangeAnimation(name, fadeDuration, loop, callbackAnimation));
	}

	private IEnumerator IEChangeAnimation(string name, float fadeDuration, bool loop, AnimationState callbackAnimation)
	{
		_isChangingAnimationState = true;

		// Get length of animation
		float length = GetAnimationDuration(name);

		// Play animation
		_animator.CrossFadeInFixedTime(name, fadeDuration);
		//_animator.Play(newState.ToString(), 0, 0.25f);

		/*
		// Wait for fadeDuration because then animation state info is changed
		yield return new WaitForSeconds(fadeDuration);

		// Get length of animation
		float length = _animator.GetCurrentAnimatorStateInfo(0).length;
		 */

		// Back to callback animation when animation is finished
		if (callbackAnimation != AnimationState.None)
		{
			yield return new WaitForSeconds(length - fadeDuration);
			PlayAnimation(callbackAnimation);
		}

		_isChangingAnimationState = false;
	}

	private IEnumerator IEWait(float wait)
	{
		_isAnimationPlaying = true;
		yield return new WaitForSeconds(wait);
		_isAnimationPlaying = false;
	}

	//private void NextAnimation()
	//{
	//	if (_currentAnimationIndex < _animations.Length - 1)
	//		_currentAnimationIndex++;
	//	else
	//		_currentAnimationIndex = 0;

	//	ChangeAnimation(_currentAnimationIndex);
	//}

	//private void PreviopusAnimation()
	//{
	//	if (_currentAnimationIndex > 0)
	//		_currentAnimationIndex--;
	//	else
	//		_currentAnimationIndex = _animations.Length - 1;

	//	ChangeAnimation(_currentAnimationIndex);
	//}
}