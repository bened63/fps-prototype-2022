using System;
using UnityEngine;

public class ProjectileSpawner : MonoBehaviour
{
	[SerializeField, Min(0)] private float fireRate = 1;
	[SerializeField, Min(0)] private int ammoCapacity = 1;
	[SerializeField] private string currentProjectileName;
	[SerializeField] private int currentProjectile = 0;
	[SerializeField] private Animator weaponAnimator;
	[SerializeField] private AudioClip reloadSound;
	[SerializeField] private AudioClip shotSound;

	//[SerializeField] private GameObject[] projectiles;
	[SerializeField] private Projectile[] _projectiles;

	[SerializeField] private Transform spawnPosition;
	[SerializeField] private float speed = 500;

	private GameManager _gameManager;
	private bool inputBlocked = false;
	private bool _isReloading = false;
	private Camera _cam;
	private WeaponSway _weaponSway;
	private bool _canShoot = false;
	private float _lastShot = 0f;
	private int _ammo = 0;
	private float _reloadTime = 0f;

	// Start is called before the first frame update
	private void Start()
	{
		currentProjectileName = _projectiles[currentProjectile].name;
		_gameManager = GameManager.instance;
		_gameManager.onDisableInput += HandleOnDisableInput;
		_cam = Camera.main;
		_weaponSway = FindObjectOfType<WeaponSway>();
		_ammo = ammoCapacity;
	}

	private void HandleOnDisableInput(bool b)
	{
		inputBlocked = b;
	}

	private RaycastHit hit;

	// Update is called once per frame
	private void Update()
	{
		if (!inputBlocked && Input.GetKeyDown(KeyCode.RightArrow))
		{
			NextEffect();
		}
		else if (!inputBlocked && Input.GetKeyDown(KeyCode.LeftArrow))
		{
			PreviousEffect();
		}
		else if (!inputBlocked && Input.GetKeyDown(KeyCode.R))
		{
			Reload();
		}

		_lastShot += Time.deltaTime;
		_canShoot = _lastShot > (1f / fireRate);

		if (!inputBlocked && Input.GetKeyDown(KeyCode.Mouse0) && _canShoot) //On left mouse down-click
		{
			// Shoot
			if (Physics.Raycast(_cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)), out hit, 2000f)) // Center of screen
			{
				Shoot();
			}
		}
		Debug.DrawRay(_cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)).origin, _cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)).direction * 100, Color.yellow);
	}

	private void Reload()
	{
		if (_isReloading)
			return;

		float duration = _projectiles[0].reloadTime;
		_isReloading = true;
		weaponAnimator.SetBool("Reload", true);
		AudioSource.PlayClipAtPoint(reloadSound, transform.position, 0.3f);
		Toolbox.Invoke(this, () =>
		{
			weaponAnimator.SetBool("Reload", false);
			_ammo = ammoCapacity;
		}, duration);

		Toolbox.Invoke(this, () =>
		{
			_isReloading = false;
		}, duration + 0.5f);
	}

	private void Shoot()
	{
		if (_isReloading)
			return;

		_lastShot = 0;

		if (_ammo <= 0)
		{
			Reload();
		}
		else
		{
			_ammo--;
			AudioSource.PlayClipAtPoint(shotSound, Camera.main.transform.position, 0.8f);

			// Projectile
			GameObject projectile = Instantiate(_projectiles[currentProjectile].prefab, spawnPosition.position, Quaternion.identity) as GameObject; //Spawns the selected projectile
			projectile.transform.LookAt(hit.point); //Sets the projectiles rotation to look at the point clicked
			projectile.transform.localScale = Vector3.one * _projectiles[currentProjectile].size;
			projectile.GetComponent<Rigidbody>().AddForce(projectile.transform.forward * _projectiles[currentProjectile].speed); //Set the speed of the projectile by applying force to the rigidbody
			Destroy(projectile, 10f);

			_weaponSway.Recoil();
		}
	}

	public void NextEffect() //Changes the selected projectile to the next. Used by UI
	{
		if (currentProjectile < _projectiles.Length - 1)
			currentProjectile++;
		else
			currentProjectile = 0;

		currentProjectileName = _projectiles[currentProjectile].name;
	}

	public void PreviousEffect() //Changes selected projectile to the previous. Used by UI
	{
		if (currentProjectile > 0)
			currentProjectile--;
		else
			currentProjectile = _projectiles.Length - 1;

		currentProjectileName = _projectiles[currentProjectile].name;
	}
}