// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "MyCollection/SphereMaskDissolveAS"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_DissolveTexture("DissolveTexture", 2D) = "white" {}
		_BaseColor("Base Color", Color) = (0.1137255,0.1215686,0.1803922,0)
		_Radius("Radius", Float) = 1
		[HDR]_BurnColor("Burn Color", Color) = (5.00778,6.931057,0.6531886,0)
		_DissolveSpherePosition("Dissolve Sphere Position", Vector) = (0,0,0,0)
		_BurnSize("Burn Size", Range( 0 , 5)) = 0.025
		_Softness("Softness", Float) = 1
		_AnimationSpeed("AnimationSpeed", Range( 0 , 5)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
		};

		uniform float4 _BaseColor;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float4 _DissolveSpherePosition;
		uniform float _Radius;
		uniform float _Softness;
		uniform sampler2D _DissolveTexture;
		uniform float _AnimationSpeed;
		uniform float _BurnSize;
		uniform float4 _BurnColor;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float3 ase_worldPos = i.worldPos;
			float temp_output_19_0 = ( ( distance( _DissolveSpherePosition , float4( ase_worldPos , 0.0 ) ) - _Radius ) / _Softness );
			float2 temp_cast_1 = (_AnimationSpeed).xx;
			float2 panner54 = ( _Time.x * temp_cast_1 + i.uv_texcoord);
			float4 tex2DNode6 = tex2D( _DissolveTexture, panner54 );
			clip( ( temp_output_19_0 - tex2DNode6.r ) );
			o.Albedo = ( _BaseColor * tex2D( _Albedo, uv_Albedo ) ).rgb;
			o.Emission = ( step( ( ( temp_output_19_0 - tex2DNode6.r ) - _BurnSize ) , _BurnSize ) * _BurnColor ).rgb;
			o.Alpha = ( _BaseColor * tex2D( _Albedo, uv_Albedo ) ).a;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18900
742;73;1672;965;2286.908;717.6326;1.190089;True;True
Node;AmplifyShaderEditor.WorldPosInputsNode;14;-2473.426,-413.6192;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector4Node;15;-2530.475,-595.6877;Inherit;False;Property;_DissolveSpherePosition;Dissolve Sphere Position;6;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;64;-2898.461,-9.984165;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;65;-2869.955,117.357;Inherit;False;Property;_AnimationSpeed;AnimationSpeed;10;0;Create;True;0;0;0;False;0;False;1;0.9421001;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.TimeNode;53;-2858.063,221.7941;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;21;-2121.714,-300.0529;Inherit;False;Property;_Radius;Radius;4;0;Create;True;0;0;0;False;0;False;1;0.87;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;16;-2195.89,-532.3862;Inherit;True;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;54;-2576.591,86.37753;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;2,2;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-2126.981,-197.9846;Inherit;False;Property;_Softness;Softness;8;0;Create;True;0;0;0;False;0;False;1;-9.33;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;20;-1925.362,-459.2997;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;19;-1776.024,-459.2583;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;6;-2206.443,64.2668;Inherit;True;Property;_DissolveTexture;DissolveTexture;2;0;Create;True;0;0;0;False;0;False;-1;None;e28dc97a9541e3642a48c0e3886688c5;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;33;-1092.319,752.0914;Inherit;False;Property;_BurnSize;Burn Size;7;0;Create;True;0;0;0;False;0;False;0.025;0.08;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;51;-1062.878,548.0241;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;12;-1041.604,-660.722;Inherit;False;Property;_BaseColor;Base Color;3;0;Create;True;0;0;0;False;0;False;0.1137255,0.1215686,0.1803922,0;0.05473477,0.05939416,0.09433961,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-1121.045,-474.1764;Inherit;True;Property;_Albedo;Albedo;1;0;Create;True;0;0;0;False;0;False;-1;None;2a09470ada0c47849ad79d26363f8e4a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;47;-774.6257,617.8156;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-718.5765,-555.4747;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;32;-715.8987,909.2547;Inherit;False;Property;_BurnColor;Burn Color;5;1;[HDR];Create;True;0;0;0;False;0;False;5.00778,6.931057,0.6531886,0;5.00778,6.931057,0.6531886,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;25;-934.9396,-161.5487;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;38;-600.2202,728.2605;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.13;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClipNode;26;-450.5557,86.4076;Inherit;False;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-401.3649,805.6564;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TriplanarNode;43;-2304.959,443.241;Inherit;True;Cylindrical;World;False;Top Texture 0;_TopTexture0;white;-1;None;Mid Texture 0;_MidTexture0;white;-1;None;Bot Texture 0;_BotTexture0;white;-1;None;Triplanar Sampler;Tangent;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT3;1,1,1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;42;-2919.161,452.7015;Inherit;True;Property;_Texture0;Texture 0;9;0;Create;True;0;0;0;False;0;False;None;e28dc97a9541e3642a48c0e3886688c5;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;292.1008,398.6728;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;MyCollection/SphereMaskDissolveAS;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.5;True;True;0;False;TransparentCutout;;AlphaTest;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;16;0;15;0
WireConnection;16;1;14;0
WireConnection;54;0;64;0
WireConnection;54;2;65;0
WireConnection;54;1;53;1
WireConnection;20;0;16;0
WireConnection;20;1;21;0
WireConnection;19;0;20;0
WireConnection;19;1;39;0
WireConnection;6;1;54;0
WireConnection;51;0;19;0
WireConnection;51;1;6;1
WireConnection;47;0;51;0
WireConnection;47;1;33;0
WireConnection;13;0;12;0
WireConnection;13;1;1;0
WireConnection;25;0;19;0
WireConnection;25;1;6;1
WireConnection;38;0;47;0
WireConnection;38;1;33;0
WireConnection;26;0;13;0
WireConnection;26;1;25;0
WireConnection;34;0;38;0
WireConnection;34;1;32;0
WireConnection;43;0;42;0
WireConnection;43;1;42;0
WireConnection;43;2;42;0
WireConnection;0;0;26;0
WireConnection;0;2;34;0
ASEEND*/
//CHKSM=982B7261E33BE511EEAC7828E7F645CBB8AC9C8C